class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }
    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }
    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get programminglang() {
        return this.lang;
    }
    set programminglang(newlang) {
        this.lang = newlang;
    }

    get salary() {
        return this._salary * 3;
    }
    set salary(newSalary) {
        this._salary = newSalary;
    }

    // get salary() {
    // return this._salary ;
    // }

    // set salary(newSalary) {
    // this._salary = newSalary * 3;
    // }
}

const programmer1 = new Programmer("Ivan Smith", 30, 950, "JavaScript");
const programmer2 = new Programmer("Taras O'Neil", 26, 825, "C++");
const programmer3 = new Programmer("Olena-Marie Bordeaux", 38, 1112, "Python");

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

console.log("-----------------------");

console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", `${programmer1.salary}$`);
console.log("Programming Language:", programmer1.lang);

console.log("-----------------------");

console.log("Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", `${programmer2.salary}$`);
console.log("Programming Language:", programmer2.lang);

console.log("-----------------------");

console.log("Programmer 3:");
console.log("Name:", programmer3.name);
console.log("Age:", programmer3.age);
console.log("Salary:", `${programmer3.salary}$`);
console.log("Programming Language:", programmer3.lang);


//Прототипне наслідування в Javascript - 
// можливість розширювати властивості одного об'єкта 
// не за рахунок перепризначення, а за рахунок 
// запозичення з іншого об'єкта
// 
//Super()треба щоб побудувати метод основі 
// батьківського метода,та налаштувати або розширити 
// функціональність. 