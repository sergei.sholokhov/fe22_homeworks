class Card {
    constructor(post) {
        this.post = post;
    }

    createCard() {
        const cardContainer = document.getElementById('news-feed');
        const cardElement = document.createElement('div');
        cardElement.classList.add('card');

        const deleteButton = document.createElement('button');
        deleteButton.classList.add('delete-button');
        deleteButton.innerText = 'Delete';

        deleteButton.addEventListener('click', () => {
            this.deletePost();
            console.log("post has been deleted");
        });

        cardElement.innerHTML = `
            <h2>${this.post.title}</h2>
            <p>${this.post.body}</p>
            <p class="person">${this.post.user.name} aka ${this.post.user.username} (${this.post.user.email})</p>
        `;

        cardElement.appendChild(deleteButton);
        cardContainer.appendChild(cardElement);
    }
    
    deletePost() {
        
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, { //${postId}
            method: 'DELETE',
        })
        .then(response => { this.removeCard(); });
    }

    removeCard() {       
        const cardById = document.getElementById(`${this.post.id}`);
        console.log(cardById); //видає null
        console.log(this.post.id); //працює

        if (cardById) {
            cardById.remove();
        }
    }
}

fetch('https://ajax.test-danit.com/api/json/users')
    .then(response => response.json())
    .then(users => {
        console.log("Users:", users);
        // console.log(users[0].username);
        // console.log(users[0].email);
        fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
            .then(posts => {
                console.log(posts);
                for (const post of posts) {
                    const user = users.find(u => u.id === post.userId);
                    post.user = user;
                    const card = new Card(post);
                    card.createCard();
                }
            })
            .catch(error => {
                console.error('Error fetching posts data:', error);
            });
    })
    .catch(error => {
        console.error('Error fetching users data:', error);
    });
