const rootDiv = document.createElement('div');

const buttonIp = document.createElement('button');
buttonIp.textContent = 'Я тебе знайду по IP';

const divData = document.createElement('div');

document.body.appendChild(rootDiv);
rootDiv.appendChild(buttonIp);
rootDiv.appendChild(divData);


buttonIp.addEventListener('click', async function () {
    try { 
        let ipResponse = await fetch('https://api.ipify.org/?format=json');
        let ipData = await ipResponse.json();
        console.log(ipData);

        let ipAddress = ipData.ip;
        console.log(ipAddress);

        let apiResponse = await fetch(`http://ip-api.com/json/${ipAddress}`); //https не спрацював 403 (Forbidden) SSL unavailable for this endpoint, order a key at https://members.ip-api.com/
        let apiData = await apiResponse.json();
        console.log(apiData);

        divData.innerHTML = `
            <p>Континент: ${apiData.continent}</p>
            <p>Країна: ${apiData.country}</p>
            <p>Регіон: ${apiData.regionName}</p>
            <p>Місто: ${apiData.city}</p>
            <p>Район: ${apiData.district}</p>
        `;

    } catch (error) { // додав try/catch не по завданню, просто зробив для себе 
        console.error('Помилка при отриманні інформації:', error);
        alert(error);
    }
});
