const API = "https://ajax.test-danit.com/api/swapi/films";

const rootDiv = document.createElement("div");

rootDiv.classList.add("episodes-container");
document.body.appendChild(rootDiv);

const sendRequest = (url, method = "GET", options) => {
    return fetch(url, { method, ...options })
        .then((response) => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .catch((error) => {
            console.error("There was a problem with the fetch operation:", error);
        });
};

const fetchCharacterNames = (characterUrls) => {
    const characterPromises = characterUrls.map((url) => sendRequest(url));
    return Promise.all(characterPromises).then((characters) => {
        console.log("Characters in Episode", characters);
        return characters.map((character) => character.name);
    });
};

const displayEpisode = (episode) => {
    console.log("Episode", episode);

    const episodeDiv = document.createElement("div");
    episodeDiv.classList.add("episode");

    const titleElement = document.createElement("h2");
    titleElement.innerHTML = `Episode ${episode.episodeId} <br> ${episode.name}`;
    episodeDiv.appendChild(titleElement);

    const openingCrawlElement = document.createElement("p");
    openingCrawlElement.textContent = `Opening Crawl: ${episode.openingCrawl}`;
    episodeDiv.appendChild(openingCrawlElement);

    const characterListParagraph = document.createElement("p");
    characterListParagraph.innerHTML = "<em>Character List in current Episode:</em>";
    episodeDiv.appendChild(characterListParagraph);

    const characterListElement = document.createElement("ul");
    episodeDiv.appendChild(characterListElement);

    fetchCharacterNames(episode.characters).then((characterNames) => {
        characterNames.forEach((characterName) => {
            const characterItemElement = document.createElement("li");
            characterItemElement.textContent = characterName;
            characterListElement.appendChild(characterItemElement);
        });
    });

    rootDiv.appendChild(episodeDiv); 
};


sendRequest(API)
    .then((data) => {
        data.forEach((series) => {
            displayEpisode(series);
        });
    });






//======================= вариант через insertAdjacentHTML ==================//

// але тут здається вивід на екран не зовсім по умові завдання:
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
// Необхідно вказати номер епізоду, назву 
// фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// const API = "https://ajax.test-danit.com/api/swapi/films";

// const rootDiv = document.createElement("div");
// rootDiv.classList.add("episodes-container");
// document.body.appendChild(rootDiv);

// const sendRequest = (url, method = "GET", options) => {
//     return fetch(url, { method, ...options })
//         .then((response) => {
//             if (!response.ok) {
//                 throw new Error(`HTTP error! Status: ${response.status}`);
//             }
//             return response.json();
//         })
//         .catch((error) => {
//             console.error("There was a problem with the fetch operation:", error);
//         });
// };

// const fetchCharacterNames = (characterUrls) => {
//     const characterPromises = characterUrls.map((url) => sendRequest(url));

//     return Promise.all(characterPromises).then((characters) => {
//         return characters.map((character) => character.name);
//     });
// };

// const displayEpisode = (episode) => {
//     const episodeDiv = document.createElement("div");
//     episodeDiv.classList.add("episode");

//     const characterList = episode.characters.map((characterUrl) => {
//         return sendRequest(characterUrl).then((character) => character.name);
//     });

//     Promise.all(characterList).then((characterNames) => {
//         const episodeHTML = `
//             <h2>Episode ${episode.episodeId} <br> ${episode.name}</h2>
//             <p>Opening Crawl: ${episode.openingCrawl}</p>
//             <p><em>Character List in current Episode:</em></p>
//             <ul class="character-list">
//                 ${characterNames.map((heroName) => `<li>${heroName}</li>`).join('')}
//             </ul>
//         `;

//         episodeDiv.insertAdjacentHTML("beforeend", episodeHTML);
//         rootDiv.appendChild(episodeDiv);
//     });
// };

// sendRequest(API)
//     .then((data) => {
//         data.forEach((series) => {
//             displayEpisode(series);
//         });
//     });
