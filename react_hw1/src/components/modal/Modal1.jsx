import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
import ModalImage from './ModalImage'



const ModalBase = ({ title, desc, handleClose, isOpen, handleOk, handleCancel }) => {

    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleClose()
        }
    }

    return (
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={handleClose} />
                    <ModalImage />
                </ModalHeader>
                <ModalBody>
                    <h2>{title}</h2>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter firstText="ADD TO FAVOURITE" secondaryText="" firstClick={handleOk} secondaryClick={handleCancel} />
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    title: PropTypes.string,
    desk: PropTypes.string,
    handleOk: PropTypes.func,
    handleCancel: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalBase