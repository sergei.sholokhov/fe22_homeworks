import React from 'react';
import './Button.scss';

const Button = ({ type, classNames, onClick, children }) => {
    
    return (
        <button
            type={type || 'button'}
            className={classNames || 'button'}
            onClick={onClick}
        >
            {children}
        </button>
    );
};

export default Button;
