import React, { useState } from 'react';
import Button from './components/button/Button';
import Modal1 from './components/modal/Modal1'
import Modal2 from './components/modal/Modal2';

const App = () => {
  const [isFirstModalOpen, setFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setSecondModalOpen] = useState(false);

  const handleFirstButton = () => alert('test First button');
  const handleSecondButton = () => alert('test Second button');

  const openFirstModal = () => {
    setFirstModalOpen(true);
    setSecondModalOpen(false);
  };

  const openSecondModal = () => {
    setFirstModalOpen(false);
    setSecondModalOpen(true);
  };

  return (
    <div className="App">
      <Button type="button" classNames="button first-button" onClick={openFirstModal}>Open first modal</Button>
      <Button type="button" classNames="button second-button" onClick={openSecondModal}>Open second modal</Button>

      {isFirstModalOpen && (
        <Modal1
          isOpen={isFirstModalOpen}
          title='Add Product "NAME"'
          desc='Description of your product'
          handleClose={()=>{setFirstModalOpen(false)}}
          handleOk={handleFirstButton}
          handleCancel={handleSecondButton}
        />
      )}

      {isSecondModalOpen && (
        <Modal2
          isOpen={isSecondModalOpen}
          title='Product Delete!'
          desc='By clicking the "Yes, Delete" button, PRODUCT NAME will be deleted.'
          handleClose={()=>{setSecondModalOpen(false)}}
          handleOk={handleFirstButton}
          handleCancel={handleSecondButton}
        />
      )}
    </div>
  );
};

export default App;

